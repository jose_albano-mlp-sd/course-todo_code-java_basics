# My Learning Path: Java Software Developer

## Course: *Java para Principiantes (Java for Beginners)* by *[TodoCode Academy](https://todocodeacademy.com/course/java-para-principiantes/)*

Hello.

Here are my Solutions to the Exercises in the Course.

### My Progress

- [x] Topic 01: Knowing Java.
- [x] Topic 02: Data Types and Variables.
- [x] Topic 03: Selective Structures (Conditional).
- [x] Topic 04: Repetitive Structures.
- [x] Topic 05: Arrays.
- [x] Topic 06: Layer Model + Graphical User Interface.
- [ ] Topic 07: Integrated Practice Assignments.
