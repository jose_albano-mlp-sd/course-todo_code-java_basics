package com.josealbano.t05_arrays.exercises;

import java.util.Scanner;

/*
  Tema 5 - Arrays: Ejercicio Nº 4

  En tres vectores diferentes se guardan los nombres de Ciudades, Temperaturas 
  Mínimas y Máximas de 5 ciudades de la provincia de Misiones.

  En el primer vector se guardan los nombres de las Ciudades, en el segundo, 
  las Temperaturas Mínimas alcanzadas en la última semana, y en el tercero, las
  Temperaturas Máximas alcanzadas en la última semana.

  Se necesita un programa que permita la carga de las Ciudades, sus 
  Temperaturas Mínimas y Máximas.
  Además, deberá poder informar por pantalla cuál fue la Ciudad con la 
  Temperatura más baja, y cuál con la Temperatura más alta (dando a conocer al
  mismo tiempo la cantidad de grados).
  
 */

/**
 *
 * @author José Albano
 */
public class TemperaturasEn3Vectores {
  public static void main(String[] args) {
    System.out.println("\n*** Tema 5 - Arrays: Ejercicio Nº 4 ***\n");
    
    Scanner entradaCadena = new Scanner(System.in);
    Scanner entradaNumerica = new Scanner(System.in);
    
    String[] ciudades = new String[5];
    double[] temperaturaMinima = new double[5];
    double[] temperaturaMaxima = new double[5];
    
    for (int i = 0; i < ciudades.length; i++) {
      System.out.printf("\nCiudad: ");
      ciudades[i] = entradaCadena.nextLine();
      
      System.out.printf("Temp. Mín.: ");
      temperaturaMinima[i] = entradaNumerica.nextDouble();
      
      System.out.printf("Temp. Máx.: ");
      temperaturaMaxima[i] = entradaNumerica.nextDouble();
    }
    
    int ciudadTempMin = -1;
    int ciudadTempMax = -1;
    double tempMin = 100;
    double tempMax = -100;
    
    for (int i = 0; i < temperaturaMinima.length; i++) {
      if (temperaturaMinima[i] < tempMin) {
        tempMin = temperaturaMinima[i];
        ciudadTempMin = i;
      }
      
      if (temperaturaMaxima[i] > tempMax) {
        tempMax = temperaturaMaxima[i];
        ciudadTempMax = i;
      }
    }
    
    System.out.println("\n" + ciudades[ciudadTempMin] + " tuvo la Temperatura "
            + "más baja de la Semana con " + temperaturaMinima[ciudadTempMin] 
            + "º C.");
    
    System.out.println(ciudades[ciudadTempMax] + " tuvo la Temperatura "
            + "más alta de la Semana con " + temperaturaMaxima[ciudadTempMax] 
            + "º C.");
    
    entradaCadena.close();
    entradaNumerica.close();
  }
}
