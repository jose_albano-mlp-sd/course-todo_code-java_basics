package com.josealbano.t05_arrays.exercises;

import java.util.Scanner;

/*
  Tema 5 - Arrays: Ejercicio Nº 5

  Realizar un programa que permita cargar con números 5 una matriz de 4x5 
  (4 filas, 5 columnas)
  
 */

/**
 *
 * @author José Albano
 */
public class RellenarMatriz {
  public static void main(String[] args) {
    System.out.println("\n*** Tema 5 - Arrays: Ejercicio Nº 5 ***\n");
    
    Scanner entrada = new Scanner(System.in);
    
    int[][] matriz = new int[4][5];
    
    for (int fila = 0; fila < matriz.length; fila++) {
      for (int columna = 0; columna < matriz[fila].length; columna++ ) {
        System.out.printf("Valor de Fila " + (fila + 1) + ", Columna " + 
                (columna + 1) + ": ");
        matriz[fila][columna] = entrada.nextInt();
      }
    }
    
    System.out.println("\nLos valores cargados son:\n");
    
    for (int fila[] : matriz) {
      for (int columna : fila) {
        System.out.printf(columna + "\t");
      }
      System.out.println();
    }
  }
}