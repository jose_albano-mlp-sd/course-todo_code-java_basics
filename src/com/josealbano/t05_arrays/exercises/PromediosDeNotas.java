package com.josealbano.t05_arrays.exercises;

import java.util.Scanner;

/*
  Tema 5 - Arrays: Ejercicio Nº 2

  En una Tabla de 4 Filas y 4 Columnas se guardan las Notas de 4 Alumnos de 
  Secundaria. Cada Fila corresponde a las Notas y al Promedio de cada Alumno.
  
  Se necesita un programa que permita a un Profesor cargar, en las 3 primeras
  posiciones (columnas) de cada fila, las Notas del Alumno, y que en la última
  columna se calculen los Promedios.

  Una vez realizados los cálculos, se desea mostrar las 3 Notas de cada Alumno
  y el Promedio correspondiente, recorriendo la matriz.
 */

/**
 *
 * @author José Albano
 */
public class PromediosDeNotas {
  public static void main(String[] args) {
    System.out.println("\n*** Tema 5 - Arrays: Ejercicio Nº 2 ***\n");
    
    Scanner entrada = new Scanner(System.in);
    double[][] notas = new double[4][4];
    double suma = 0;
    
    for (int alumno = 0; alumno < notas.length; alumno++) {
      for (int nota = 0; nota < notas[alumno].length - 1; nota++ ) {
        System.out.print("Ingrese Nota " + (nota + 1) + " para el Alumno " +
                (alumno + 1) + ": ");
        notas[alumno][nota] = entrada.nextDouble();
        
        suma += notas[alumno][nota];
      } // for notas
      
      notas[alumno][notas.length - 1] = suma / (notas.length - 1);
      suma = 0;
      System.out.println();
      
    } // for alumno
    
    System.out.println("Alumno\tNota 1\tNota 2\tNota 3\tPromedio");
    
    for (int alumno = 0; alumno < notas.length; alumno++ ) {
      System.out.print(alumno + 1 + "\t");
      
      for (int nota = 0; nota < notas[alumno].length; nota++) {
        System.out.print(notas[alumno][nota] + "\t");
      }
      
      System.out.println();
    }
    
    entrada.close();
  }
}