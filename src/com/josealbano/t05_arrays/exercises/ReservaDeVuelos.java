package com.josealbano.t05_arrays.exercises;

import java.util.Scanner;

/*
  Tema 5 - Arrays: Ejercicio Nº 4

  Una compañia de vuelos cuenta con 6 destinos a los cuales realiza 3 vuelos
  diariamente, uno por la mañana, uno por la tarde, y uno por la noche. Para
  administrar estos datos, utiliza una matriz, donde cada fila es un destino
  y en cada columna se guarda la cantidad de asientos disponibles. Por ejemplo:
  
                      0       1         2
                      Mañana  Mediodia  Noche
    0 Rio de Janeiro  120     15        30
    1 Cancún          65      1         2
    2 Madrid          4       52        7
    3 Roma            46      32        16
    4 Milán           64      23        11
    5 Iguazú          61      12        91
  
  1)  El programa debe permitir la carga de la matriz con la Cantidad de 
      Asientos para cada Vuelo.

  2)  Al mismo tiempo, el programa debe permitir a un usuario ingresar el 
      Número de Destino, el Número de Vuelo (Mañana, Tarde, Noche), y la 
      Cantidad de Pasajes que necesita.

  3)  A partir de la solicitud del usuario, el programa debe controlar si hay
      Cantidad suficiente de Asientos para la Cantidad de Pasajes ingresada. En
      caso de que así sea, se debe informar con un Cartel por pantalla que diga
      "Su Reserva fue realizada con Éxito", y se debe descontar del Total, los 
      Asientos solicitados por el usuario. En caso de no haber más Asientos 
      disponibles, se debe informar con otro Cartel que diga "Disculpe, No se 
      pudo completar su Reserva, dado que no hay Asientos disponibles".

  Desde la compañía, manifiestan que NO CONOCEN cuántas Ventas/Reservas se 
  hacen por día. Por lo cual, para finalizar las ventas, se ingresa la palabra
  "finish".
  
 */
/**
 *
 * @author José Albano
 */
public class ReservaDeVuelos {

  public static void main(String[] args) {
    System.out.println("\n*** Tema 5 - Arrays: Ejercicio Nº 6 ***\n");

    Scanner entrada = new Scanner(System.in);

    int[][] asientos = new int[6][3];

    System.out.println("Cargue la Cantidad de Asientos disponibles para cada "
            + "Destino y Horario:\n");

    for (int destino = 0; destino < asientos.length; destino++) {
      System.out.println("Destino: " + getNombreDestino(destino) + "\n");

      for (int horario = 0; horario < asientos[destino].length; horario++) {
        System.out.printf("\t" + getHorario(horario) + ": ");

        asientos[destino][horario] = entrada.nextInt();
      } // for horario

      System.out.println();
    } // for destino

    String centinela = "";
    Scanner entradaCadena = new Scanner(System.in);

    while (!centinela.equalsIgnoreCase("finish")) {

      System.out.println("\n*** Realizar Reserva ***\n\n"
              + "\t1 - Rio de Janeiro"
              + "\n\t2 - Cancún"
              + "\n\t3 - Madrid"
              + "\n\t4 - Roma"
              + "\n\t5 - Milán"
              + "\n\t6 - Iguazú\n");

      System.out.printf("\t\tNúmero de Destino: ");
      int destino = entrada.nextInt() - 1;

      System.out.println("\n\t1 - Mañana"
              + "\n\t2 - Mediodía"
              + "\n\t3 - Noche\n");
      System.out.printf("\t\tHorario: ");
      int horario = entrada.nextInt() - 1;

      System.out.printf("\n\t\tCantidad de Pasajes: ");
      int pasajes = entrada.nextInt();

      if (destino >= 0 && destino <= 5) {
        if (horario >= 0 && horario <= 2) {
          
          if (pasajes <= asientos[destino][horario]) {
            System.out.println("\n\tSu Reserva fue realizada con Éxito");
            asientos[destino][horario] -= pasajes;

          } else {
            System.out.println("\n\tDisculpe, no se pudo completar su Reserva. "
                    + "No hay Asientos disponible(s) para:");
            System.out.println("\n\t\t" + getNombreDestino(destino) + ", "
                    + getHorario(horario));
          }
        
        } else {
          System.out.println("\n\t" + getHorario(horario) + ".");
        }
        
      } else {
        System.out.println("\n\t" + getNombreDestino(destino) + ".");
      }

      System.out.printf("\n\t¿Continua haciendo Reservas?\n\t(Ingrese "
              + "\"finish\" para finalizar ó Cualquier otro valor para "
              + "continuar): ");
      centinela = entradaCadena.nextLine();

    } // while centinela

  } // main

  public static String getNombreDestino(int destino) {
    switch (destino) {
      case 0:
        return "Rio de Janeiro";
      case 1:
        return "Cancún";
      case 2:
        return "Madrid";
      case 3:
        return "Roma";
      case 4:
        return "Milán";
      case 5:
        return "Iguazú";
      default:
        return "Destino Inexistente";
    }
  } // getNombreDestino

  public static String getHorario(int horario) {
    switch (horario) {
      case 0:
        return "Mañana";
      case 1:
        return "Mediodía";
      case 2:
        return "Noche";
      default:
        return "Horario Inexistente";
    }
  } // getHorario
}
