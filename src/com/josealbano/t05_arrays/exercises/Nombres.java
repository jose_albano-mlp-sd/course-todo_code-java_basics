package com.josealbano.t05_arrays.exercises;

import java.util.Scanner;

/*
  Tema 5 - Arrays: Ejercicio Nº 3

  Realizar un programa que incluya un vector que almacene 8 nombres.

  Realizar un recorrido del vector para cargar los Nombres, y otro recorrido
  para mostrar los mismos por pantalla.
 */

/**
 *
 * @author José Albano
 */
public class Nombres {
  public static void main(String[] args) {
    System.out.println("\n*** Tema 5 - Arrays: Ejercicio Nº 3 ***\n");
    
    Scanner entrada = new Scanner(System.in);
    
    String[] nombres = new String[8];
    
    for (int i = 0; i < nombres.length; i++) {
      System.out.print("Ingrese un Nombre: ");
      nombres[i] = entrada.nextLine();
    }
    
    System.out.println("\nLos Nombres ingresados son:\n");
    
    for (String nombre : nombres) {
      System.out.println("\t" + nombre);
    }
    
    entrada.close();
  }
}