package com.josealbano.t05_arrays.exercises;

import java.util.Scanner;

/*
  Tema 5 - Arrays: Ejercicio Nº 1

  Realizar un programa que permita cargar 15 números en un vector. Una vez 
  cargados, se necesita que el programa cuente e informe por pantalla cuántas
  veces se cargó el número 3.
 */

/**
 *
 * @author José Albano
 */
public class NumeroEspecifico {
  
  public static void main(String[] args) {
    
    System.out.println("\n*** Tema 5 - Arrays: Ejercicio Nº 1 ***\n");
    
    int[] numeros = new int[15];
    Scanner entrada = new Scanner(System.in);
    int contadorNumeros = 0;
    
    for (int i = 0; i < numeros.length; i++) {
      System.out.print("Ingrese un Valor Entero en la posición " + i + ": ");
      numeros[i] = entrada.nextInt();
    }
    
    
    for (int i : numeros) {
      if (3 == i) {
        contadorNumeros++;
      }
    }
    
    System.out.println("\nEl Número 3, se cargó " + contadorNumeros + 
            ( (contadorNumeros > 1) ? " veces." : " vez.") );
    
    entrada.close();
  }
}