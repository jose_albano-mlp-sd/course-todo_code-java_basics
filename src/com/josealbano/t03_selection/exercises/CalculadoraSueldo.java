package com.josealbano.t03_selection.exercises;

import java.util.Scanner;

/*
  Una despensa desea calcular los sueldos de sus empleados. Los puestos de los
  mismos pueden tener 3 categorías: 1) repositor, 2) cajero, y 3) supervisor

  - Los repositores cobran $15.890 + un bono del 10%
  - Los cajeros cobran $25.630,89
  - Los supervisores cobran $35.560,20 en bruto al cual se le descuenta un 11%
    de jubilación.
  
  Se necesita un programa que dependiendo del tipo de empleado del que se 
  trate, calcule y muestre en pantalla el sueldo correspondiente.
 */

/**
 *
 * @author José Albano
 */
public class CalculadoraSueldo {
  public static void main(String[] args) {
    int categoriaEmpleado;
    double sueldo = 0;
    
    Scanner entrada = new Scanner (System.in);
        
    System.out.println("Categorías de Empleado:");
    System.out.println("\t1) Repositor\n\t2) Cajero\n\t3) Supervisor");
    System.out.println("\t\nIngrese la Categoría: ");
    
    categoriaEmpleado = entrada.nextInt();
    
    switch(categoriaEmpleado) {
      case 1: 
        sueldo = 15890 + 15890 * .10;
        break;
        
      case 2:
        sueldo = 25630.89;
        break;
        
      case 3:
        sueldo = 35560.20 - 35560.20 * .11;
        break;
        
      default:
        System.out.println("\n\tDebe ingresar un Número de Categoría válido.");
    }
    
    if (categoriaEmpleado >= 1 && categoriaEmpleado <= 3) {
      System.out.println("\n\tEl Sueldo de la Categoría " + categoriaEmpleado + 
      " es: $" + sueldo);
    }
  } 
}