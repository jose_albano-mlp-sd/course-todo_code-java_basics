package com.josealbano.t02_data_types;

/**
 *
 * @author José Albano
 */
public class Variables {

  public static void main(String[] args) {
    int number = 5;
    double temperature = 25.7;
    long id = 123456789;

    String nombre = "José";
    char letter = 'A';

    boolean yesOrNo = false;
  }
}
