package com.josealbano.t02_data_types.exercises;

/*
  Realizar un programa que permita el intercambio de valores entre dos variables.
  Una vez realizado el intercambio, mostrar el resultado por pantalla.

  Por ejemplo: Si la variable numero1 vale 35, y la variable numero2 vale 20, 
  realizar las acciones necesarias para que numero1 pase a valer 20, y numero2
  pase a valer 35.
  
 */

/**
 *
 * @author José Albano
 */
public class ValuesInterchange {
  public static void main(String[] args) {
    int number1 = 35;
    int number2 = 20;
    int aux = 0;
    
    System.out.println("Initial values of the variables:");
    System.out.println("Number 1: " + number1);
    System.out.println("Number 2: " + number2);
    
    aux = number1;
    number1 = number2;
    number2 = aux;
    
    System.out.println();
    
    System.out.println("Final values of the variables:");
    System.out.println("Number 1: " + number1);
    System.out.println("Number 2: " + number2);
  }
}
