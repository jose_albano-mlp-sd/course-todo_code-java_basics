package com.josealbano.t02_data_types;

/**
 *
 * @author José Albano
 */
public class Operators {
  public static void main(String[] args) {
    int number1, number2, result;
    
    number1 = 4;
    number2 = 2;
    
    result = number1 + number2;
    System.out.println("The Result of the Sum is: " + result);
    
    result = number1 - number2;
    System.out.println("The Result of the Substraction is: " + result);
    
    result = number1 * number2;
    System.out.println("The Result of the Multiplicación is: " + result);
    
    result = number1 / number2;
    System.out.println("The Result of the Division is: " + result);
  }
}