package com.josealbano.t07_final_assignment.address_book.logic;

import com.josealbano.t07_final_assignment.address_book.gui.MainGui;

/* 
  Ejercicio Práctico Integrador: Agenda Electrónica
  
  Se necesita un programa para gestionar los Contactos de una Agenda Electrónica
  
  a)  En la Agenda se debe permitir ingresar los siguientes datos:
      DNI, Apellido, Nombre, Dirección, Teléfono y Fecha de Nacimiento (Cada 
      uno con su correspondiente TextField).

  b)  Se necesita almacenar los datos de 10 personas. Para ello se propone la 
      utilización de un vector para almacenar los valores de cada uno de los 
      campos (teniendo en cuenta que pueden existir distintos tipos de datos), 
      en donde cada índice indicará los datos de una determinada persona en 
      todos los vectores. Por ejemplo:

          |   dni    |  |  nombres  |   |  apellidos  |   |   etc.   |
          |----------|  |-----------|   |-------------|   |----------|
          | 36051447 |  |  Luisina  |   |  De Paula   |   |   ...    |
          | 34825697 |  | Guillermo |   |    Oink     |   |   ...    |
          | 14899589 |  |  Esteban  |   |  Cugliari   |   |   ...    |
          |   ...    |  |    ...    |   |     ...     |   |   ...    |

  c)  El programa debe permitir guardar valores en una determinada posición del
      vector (al hacer clic en el bóton "Guardar"), y recorrer el vector 
      encontrando los datos guardados.
*/

/**
 *
 * @author José Albano
 */
public class AddressBookApp {
  public static void main(String[] args) {
    MainGui gui = new MainGui();
    gui.setVisible(true);
    gui.setLocationRelativeTo(null);
  }
}