package com.josealbano.t07_final_assignment.box_office.logic;

import java.util.Scanner;

/**
 *
 * @author José Albano
 */
public class TheaterTicketsApp {

  public static void main(String[] args) {
    System.out.println("*** Final Assignment ***\n"
            + "\tTheater Tickets\n");
    Scanner input = new Scanner(System.in);

    char[][] seats = new char[10][10];
    boolean endApp = false;

    for (char[] seat : seats) {
      for (int column = 0; column < seat.length; column++) {
        seat[column] = 'L';
      }
    }

    seats[3][7] = 'X';

    int rowToReserve = 0;
    int seatToReserve = 0;

    System.out.println("\nBuy a Ticket!");

    System.out.println("\n\tDo you want to Reserve a Seat?");
    System.out.printf("\t\t1 - Yes\n\t\t0 - Exit of App:\n\n\tYour answer: ");
    endApp = input.nextInt() != 1;
    
    
    while (!endApp) {

      System.out.printf("\n\tEnter the Row: ");
      rowToReserve = input.nextInt();

      System.out.printf("\tEnter the Seat Number: ");
      seatToReserve = input.nextInt();

      if (rowToReserve >= 1 && rowToReserve <= 10) {

        if (seatToReserve >= 0 && seatToReserve <= 10) {

          if (seats[rowToReserve - 1][seatToReserve - 1] == 'L') {
            seats[rowToReserve - 1][seatToReserve - 1] = 'X';

            System.out.println("\n\tReservation made successfully.");

          } else {
            System.out.println("\n\tPlease, select another seat, this one is "
                    + "already sold");
          }

          System.out.println();
          for (char[] row : seats) {
            System.out.printf("\t\t");
            for (char column : row) {
              System.out.printf(column + " ");
            }
            System.out.println();
          }
        } else {
          System.out.println("\n\tInvalid Seat: " + seatToReserve + "\n\tThe "
                + "Row " + rowToReserve + " Only have 10 Seats.");
        }
      } else {
        System.out.println("\n\tInvalid Row: " + rowToReserve + "\n\tThe "
                + "Theater Only have 10 rows of Seats.");
      }

      System.out.println("\n\tDo you want to Reserve a Seat?");
      System.out.printf("\t\t1 - Yes\n\t\t0 - Exit of App:\n\n\tYour answer: ");
      endApp = input.nextInt() != 1;

    } // while
  }
}
