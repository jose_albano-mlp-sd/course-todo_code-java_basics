package com.josealbano.t04_iteration.exercises;

import java.util.Scanner;

/*
  En la Ciudad de Oberá, Misiones, se realizar todos los años la "Maratón del
  Inmigrante" en el marco de la Fiesta Nacional del Inmigrante.
  
  El evento cuenta con un día de inscripciones el día anterior a la carrera, 
  por lo que se desconoce la cantidad exacta de inscriptos que puede llegar a 
  haber.
  
  Desde la Federación de Colectividades (organizador), manifestaron que 
  solicitan los siguientes datos para la inscripción de cada participante: DNI,
  Nombre y Edad.
  
  Las categorías a las que se puede inscribir son las siguientes:
  - Menores A (6 a 10 años)
  - Menores B (11 a 17 años)
  - Juveniles (18 a 30 años)
  - Adultos (31 a 50 años)
  - Adultos Mayores (mayores de 50 años)

  Se necesita un programa, que a partir del ingreso de los datos y edad de cada 
  participante, muestre por pantalla a qué categoría debe ser inscripto. 
  Cabe destacar, que al finalizar el día, para dar fin a las inscripciones, se
  debe ingresar un DNI con el valor 0, y un nombre con la palabra "fin".
 */

/**
 *
 * @author José Albano
 */
public class MaratonDelInmigrante {
  public static void main(String[] args) {
    String dni;
    String nombre;
    int edad;
    String categoria = "";
    
    Scanner entrada = new Scanner (System.in);
    Scanner entradaNumerica = new Scanner (System.in);
            
    System.out.println("DNI: (\"0\" para terminar): ");
    dni = entrada.nextLine();
    
    System.out.println("Nombre: (\"fin\" para terminar): ");
    nombre = entrada.nextLine();
    
    while ( !dni.equals("0") && !nombre.equalsIgnoreCase("fin") ) {
    
      System.out.println("Edad: ");
      edad = entradaNumerica.nextInt();

      if (edad > 50 ) {
        categoria = "Categoria: Adultos Mayores";
      } else if (edad > 30 ) {
        categoria = "Categoria: Adultos";
      } else if (edad > 18 ) {
        categoria = "Categoria: Juveniles";
      } else if (edad > 11 ) {
        categoria = "Categoria: Menores B";
      } else if (edad > 6 ) {
        categoria = "Categoria: Menores A";
      } else {
        categoria = "NO PUEDE PARTICIPAR";
      }

      System.out.println("\nParticipante " + nombre + ", DNI " + dni + 
              ", Edad " + edad + ": \"" + categoria + "\".\n");

      System.out.println("DNI: (\"0\" para terminar): ");
      dni = entrada.nextLine();

      System.out.println("Nombre: (\"fin\" para terminar): ");
      nombre = entrada.nextLine();
    }
  }
}