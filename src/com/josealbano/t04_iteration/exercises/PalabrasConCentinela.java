package com.josealbano.t04_iteration.exercises;

import java.util.Scanner;

/*
  Realizar un programa que muestre en pantalla palabras que sean ingresadas por
  teclado hasta que se ingrese la palabra "salir".
 */

/**
 *
 * @author José Albano
 */
public class PalabrasConCentinela {
  public static void main(String[] args) {
    Scanner entrada = new Scanner (System.in);
        
    System.out.println("Ingrese una Palabra (\"salir\" para terminar): ");
    String palabra = entrada.next();
    
    while (!palabra.equalsIgnoreCase("salir")) {
      System.out.println(palabra);
            
      System.out.println("Ingrese una Palabra (\"salir\" para terminar): ");
      palabra = entrada.next();
    }
  }
}