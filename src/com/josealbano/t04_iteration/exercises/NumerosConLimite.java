package com.josealbano.t04_iteration.exercises;

import java.util.Scanner;

/*
  Realizar un programa que dado por teclado un Límite Numérico (por ejemplo
  100), muestre en pantalla todos los números hasta el Límite introducido 
  (empezando por 1)
 */

/**
 *
 * @author José Albano
 */
public class NumerosConLimite {
  public static void main(String[] args) {
    Scanner entrada = new Scanner (System.in);
        
    System.out.println("Ingrese un Número Entero Positivo como Límite: ");
    
    int limite = entrada.nextInt();
    
    int contador = 1;
    
    while (contador <= limite) {
      System.out.println(contador);
      
      contador++;
    }
  }
}