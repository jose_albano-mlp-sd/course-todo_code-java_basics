package com.josealbano.t04_iteration.exercises;
/*
  Dibujar en una terminal el siguiente Árbol de Navidad con altura variable:

     *
    ***
   *****
  *******
    |||

       *
      ***
     *****
    *******
   *********
  ***********
      |||
 */

/**
 *
 * @author José Albano
 */
public class ArbolNavidad {
  public static void main(String[] args) {
    int altura = 4;
    
    for (int fila = 1; fila <= altura; fila++) {
      for (int espacios = altura; espacios > fila; espacios--) {
        System.out.print(" ");
      }
      
      for (int asteriscos = 1; asteriscos < fila * 2 ; asteriscos++) {
        System.out.print("*");
      }
      
      System.out.println();
    }
    
    for (int i = 1; i <= altura - 2; i++) {
      System.out.print(" ");
    }
    
    System.out.println("|||");
  }
}